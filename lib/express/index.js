
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var express     = require("express");
var http        = require("http");
var config      = require("../config/config");
var middleware  = require("../middleware");
var app         = express();
var router      = express.Router();
var logger      = require(appRootPath + "/lib/logger/logger");

// ------- auth related stuff

var passport   = require("../authentication/passport");

var flash        = require("connect-flash");
var bodyParser   = require("body-parser");
var session      = require("express-session");
var cookieParser = require("cookie-parser");
var RedisStore   = require("connect-redis")(session);

app.use(cookieParser());

app.use(bodyParser.json({
  extended: true
}));

app.use(session({
  resave : config.get("session:resave"),
  saveUninitialized: config.get("session:saveUninitialized"),
  secret: config.get("session:secret"),
  store : new RedisStore({
    host : config.get("session:store:host"),
    port : config.get("session:store:port"),
    user : config.get("session:store:user"),
    pass : config.get("session:store:pass")
  }),
  cookie : {
    maxAge : config.get("session:cookie:maxAge")
  }
}));

// Initialize Passport!  Also use passport.session() middleware, to support
// persistent login sessions (recommended).
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

// ------- end of auth stuff

// add query builder object for every request
app.all("*", middleware.requestMiddleware.validate);

app.set("port", config.get("express:port"));

app.use("/static", express.static(appRootPath + "/public"));

// load our routes
require("./../../app/routes.js")(router, passport);

app.use("/", router);

router.param("id", middleware.validatorMiddleware.id);

app.use(middleware.notFoundMiddleware);
app.use(middleware.errorHandlerMiddleware);

http.createServer(app).listen(app.get("port"));
module.exports = app;

logger.info("Kogo is now running on port " + app.get("port"));
