
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

var Entity = require(appRootPath + "/lib/entity");
var util  = require("util");

/**
 * TicketActivity entity class definition
 */
function TicketActivity(data) {
  data = data || false;
  if (data) this.inflate(data);
};

/**
 * TicketActivity extends Entity
 */
util.inherits(TicketActivity, Entity);

TicketActivity.prototype.id = "";
TicketActivity.prototype.ticketId = "";
TicketActivity.prototype.type = "";
TicketActivity.prototype.field = "";
TicketActivity.prototype.originalValue = "";
TicketActivity.prototype.newValue = "";
TicketActivity.prototype.userId = "";
TicketActivity.prototype.createdAt = "";
TicketActivity.prototype.updatedAt = "";

TicketActivity.prototype.getFields = function () {
  return [
    "id",
    "ticketId",
    "type",
    "field",
    "originalValue",
    "newValue",
    "userId",
    "createdAt",
    "updatedAt"
  ];
};

TicketActivity.prototype.getId = function () {
  return this.id;
}

TicketActivity.prototype.setId = function (id) {
  this.id = id;
}

module.exports = TicketActivity;
