
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

var TicketActivitiesService = require("../service/ticketActivities");
var logger                     = require(appRootPath + "/lib/logger/logger");
var errorHandler        = require(appRootPath + "/lib/error/errorHandler");

exports.get = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var ticketActivitiesService = new TicketActivitiesService();
  ticketActivitiesService.all(req.query)
    .then(function (ticketActivities) {
      return res.status(200).json(ticketActivities);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

exports.getById = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var ticketActivitiesService = new TicketActivitiesService();
  ticketActivitiesService.get(req.params.id)
    .then(function (ticketActivity) {
      if (!ticketActivity) return res.status(404).json("Not Found");
      return res.status(200).json(ticketActivity);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

exports.create = function (req, res) {
  logger.info(req.method + " request" + req.url);

  var ticketActivitiesService = new TicketActivitiesService();
  ticketActivitiesService.create(req.body)
    .then(function (ticketActivity) {
      if (ticketActivity === null) return res.status(404).json("Not Found");
      res.setHeader("Location", "/ticket-activities/" +  ticketActivity.getId());
      return res.status(201).json(ticketActivity.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

exports.update = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var ticketActivitiesService = new TicketActivitiesService();
  ticketActivitiesService.update(req.body)
    .then(function (ticketActivity) {
      if (ticketActivity === null) return res.status(409).json("Conflict");
      return res.status(200).json(ticketActivity.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

exports.replace = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var ticketActivitiesService = new TicketActivitiesService();
  ticketActivitiesService.replace(req.body)
    .then(function (ticketActivity) {
      if (ticketActivity === null) return res.status(409).json("Conflict");
      return res.status(200).json(ticketActivity.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

exports.delete = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var ticketActivitiesService = new TicketActivitiesService();
  ticketActivitiesService.del(req.params.id)
    .then(function (numberOfDeleted) {
      if (numberOfDeleted === 0) return res.status(404).json("Not Found");
      return res.status(204).json("No Content");
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};
