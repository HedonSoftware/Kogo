
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all Ticket Activity service related files
 */

// export all routes
module.exports.Routes = {
  TicketActivities: require("./lib/routes/ticketActivities")
};

// export all services
module.exports.Services = {
  TicketActivities: require("./lib/service/ticketActivities")
};

// export all mappers
module.exports.Mappers = {
  TicketActivities: require("./lib/mappers/ticketActivities")
};

// export all entities
module.exports.entities = {
  TicketActivity: require("./lib/entity/ticketActivity")
};
