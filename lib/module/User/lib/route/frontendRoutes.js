
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var logger      = require(appRootPath + "/lib/logger/logger");
var path        = require("path");
var _           = require(appRootPath + "/lib/utility/underscore");
var config      = require(appRootPath + "/lib/config/config");

module.exports.getDetails = function (req, res) {
  logger.info("User::FrontendRoutes::get - request received", {
    httpMethod: req.method,
    url: req.url,
    query: req.query.export()
  });

  return res.status(200).json(req.user);
};

module.exports.uploadAvatar = function (req, res) {
  var data = _.pick(req.body, "type");
  var uploadPath = path.normalize(config.get("uploads:dir"));
  var file = req.files.file;
  // console.log(file.name); //original name (ie: sunset.png)
  // console.log(file.path); //tmp path (ie: /tmp/12345-xyaz.png)
  // console.log(uploadPath); //uploads directory: (ie: /home/user/data/uploads)

  return res.status(200).json(file);
};
