
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var UserEntity  = require("../entity/userEntity");
var Request     = require(appRootPath + "/lib/request/request");
var _           = require(appRootPath + "/lib/utility/underscore");

class UsersService
{
  constructor(userGateway)
  {
    if (!userGateway) {
      var UsersGateway = require("../gateway/user/httpUserGateway");
      userGateway = new UsersGateway();
    }

    this.userGateway = userGateway;
  }

  /**
   * Method gets all users matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  all(query)
  {
    query = query || new Request();
    return this.userGateway.fetchAll(query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single user by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  get(id)
  {
    var query = new Request();
    query.setConditions({"id": id});

    return this.all(query)
      .then(function (users) {
        if (!_.isArray(users) || _.isEmpty(users)) {
          return null;
        }
        return users.shift();
      });
  }

  create(data)
  {
    var user = new UserEntity(data);
    return this.userGateway.insert(user);
  }

  update(data)
  {
    var user = new UserEntity(data);
    return this.userGateway.update(user);
  }

  replace(data)
  {
    var user = new UserEntity(data);
    return this.userGateway.replace(user);
  }

  delete(id)
  {
    return this.userGateway.delete(id);
  }
}

module.exports = UsersService;
