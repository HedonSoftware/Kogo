
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var LaneEntity  = require("../entity/laneEntity");
var Request     = require(appRootPath + "/lib/request/request");
var _           = require(appRootPath + "/lib/utility/underscore");

class LanesService
{
  constructor(laneGateway, ticketGateway)
  {
    if (!laneGateway) {
      var LaneGateway = require("../gateway/lane/httpLaneGateway");
      laneGateway = new LaneGateway();
    }

    if (!ticketGateway) {
      var TicketGateway = require(appRootPath + "/lib/module/Ticket/lib/gateway/ticket/httpTicketGateway");
      ticketGateway = new TicketGateway();
    }

    this.laneGateway = laneGateway;
    this.ticketGateway = ticketGateway;
  }

  /**
   * Method gets all lanes matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  all(projectId, boardId, query)
  {
    query = query || new Request();
    if (!_.isEmpty(query.getFields())) {
      query.appendField("tags");
    }
    var me = this;
    var lanes;
    return this.laneGateway.fetchAll(projectId, boardId, query)
      .then(function (data) {

        if (!_.isArray(data)) {
          return [];
        }

        lanes = data;
        var laneTags = [];
        _.forEach(lanes, function(lane) {
          laneTags.push(lane.tags);
        });

        laneTags = _.flatten(laneTags);
        var request = new Request({
          fields: {
            "uc.username": "creator",
            "uc.firstName": "creatorFirstName",
            "uc.lastName": "creatorLastName",
            "uc.email": "creatorEmail",
            "uc.avatar": "creatorAvatar",
            "ua.username": "assignee",
            "ua.firstName": "assigneeFirstName",
            "ua.lastName": "assigneeLastName",
            "ua.email": "assigneeEmail",
            "ua.avatar": "assigneeAvatar",
            0: "storyPoints",
            1: "id",
            2: "name",
            3: "tags",
            4: "description"
          },
          conditions: {
            tags: {
              contains: _.flatten(laneTags)
            }
          },
          joins: [
            "assignee",
            "creator"
          ]
        });

        return me.ticketGateway.fetchAll(request);
      }).then(function(tickets) {

        _.forEach(lanes, function(lane) {
          _.forEach(tickets, function(ticket) {
            if (!_.isEmpty(_.intersection(ticket.tags, lane.tags))) {
              if (!_.isArray(lane.tickets)) {
                lane.tickets = [];
              }

              lane.tickets.push(ticket);
            }
          });
        });

        return lanes;

      });
  }

  /**
   * Method gets single lane by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  fetchOne(projectId, boardId, query)
  {
    return this.all(projectId, boardId, query)
      .then(function (lanes) {
        if (!_.isArray(lanes) || _.isEmpty(lanes)) {
          return null;
        }
        return lanes.shift();
      });
  }

  create(projectId, boardId, data)
  {
    var lane = new LaneEntity(data);
    return this.laneGateway.insert(projectId, boardId, lane);
  }

  update(projectId, boardId, data)
  {
    var lane = new LaneEntity(data);
    return this.laneGateway.update(projectId, boardId, lane);
  }

  replace(projectId, boardId, data)
  {
    var lane = new LaneEntity(data);
    return this.laneGateway.replace(projectId, boardId, lane);
  }

  delete(projectId, boardId, laneId)
  {
    return this.laneGateway.delete(projectId, boardId, laneId);
  }
}

module.exports = LanesService;
