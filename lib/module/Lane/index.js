
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all Lane service related files
 */

// export all routes
module.exports.Routes = {
  LaneRoutes: require("./lib/route/laneRoutes")
};

// export all services
module.exports.Services = {
  LanesService: require("./lib/service/lanesService")
};

// export all gateways
module.exports.Gateways = {
  RethinkDbLaneGateway: require("./lib/gateway/lane/httpLaneGateway")
};

// export all entities
module.exports.Entities = {
  LaneEntity: require("./lib/entity/laneEntity")
};
