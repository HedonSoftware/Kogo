
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

/**
 * This file will load all Project service related files
 */

"use strict";

// export all routes
module.exports.Routes = {
  ProjectRoutes: require("./lib/route/projectRoutes")
};

// export all services
module.exports.Services = {
  ProjectsService: require("./lib/service/projectsService")
};

// export all gateways
module.exports.Gateways = {
  HttpProjectGateway: require("./lib/gateway/project/httpProjectGateway")
};

// export all entities
module.exports.Entities = {
  ProjectEntity: require("./lib/entity/projectEntity")
};
