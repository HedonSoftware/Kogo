
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath   = require("app-root-path");
var ProjectEntity = require("../entity/projectEntity");
var Request       = require(appRootPath + "/lib/request/request");
var _             = require(appRootPath + "/lib/utility/underscore");
var ServiceError  = require(appRootPath + "/lib/service/serviceError");

class ProjectsService
{
  constructor(projectGateway, ticketGateway)
  {
    if (!projectGateway) {
      var ProjectGateway = require("../gateway/project/httpProjectGateway");
      projectGateway = new ProjectGateway();
    }

    if (!ticketGateway) {
      var TicketGateway = require(appRootPath + "/lib/module/Ticket/lib/gateway/ticket/httpTicketGateway");
      ticketGateway = new TicketGateway();
    }

    this.projectGateway = projectGateway;
    this.ticketGateway = ticketGateway;
  }

  /**
   * Method gets all projects matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  all(query)
  {
    query = query || new Request();
    var me = this;
    var projects;
    return this.projectGateway.fetchAll(query)
      .then(function (data) {

        if (!_.isArray(data)) {
          return [];
        }

        projects = data;
        var tags = [];
        var projectTags = [];

        _.forEach(data, function(project) {
          project.boardsCounter = _.keys(project.boards).length;
          projectTags = [];
          _.forEach(project.boards, function(board) {
            _.forEach(board.lanes, function(lane) {
              tags.push(lane.tags);
              projectTags.push(lane.tags);
            });
          });
          project.ticketTags = _.flatten(projectTags);
          project.ticketsCounter = 0;
        });

        var request = new Request({
          conditions: {
            tags: {
              contains: _.flatten(tags)
            }
          }
        });

        return me.ticketGateway.fetchAll(request);

      }).then(function(tickets) {

        if (!_.isArray(tickets)) {
          return projects;
        }

        _.forEach(projects, function(project) {
          _.forEach(tickets, function(ticket) {
            _.forEach(ticket.tags, function(tag) {
              if (_.contains(project.ticketTags, tag)) {
                project.ticketsCounter++;
              }
            });
          });
        });

        return projects;

      });
  }

  /**
   * Method gets single project by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  fetchOne(query)
  {
    return this.all(query)
      .then(function (projects) {
        if (!_.isArray(projects) || _.isEmpty(projects)) {
          return null;
        }
        return projects.shift();
      });
  }

  create(data)
  {
    var project = new ProjectEntity(data);

    if (!_.isEmpty(project.boards)) {
      throw new ServiceError(
        "Unable to insert project with boards. " +
        "Use /projects/:projectId/boards to insert board",
        data,
        409
      );
    }

    return this.projectGateway.insert(project);
  }

  update(data)
  {
    var project = new ProjectEntity(data);

    if (!_.isEmpty(project.boards)) {
      throw new ServiceError(
        "Unable to update project with boards. " +
        "Use /projects/:projectId/boards/:boardId to update board",
        data,
        409
      );
    }

    return this.projectGateway.update(project);
  }

  replace(data)
  {
    var project = new ProjectEntity(data);
    return this.projectGateway.replace(project);
  }

  delete(id)
  {
    return this.projectGateway.delete(id);
  }
}

module.exports = ProjectsService;
