
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var TagEntity   = require("../entity/tagEntity");
var Request     = require(appRootPath + "/lib/request/request");
var _           = require(appRootPath + "/lib/utility/underscore");

class TagsService
{
  constructor(tagGateway)
  {
    if (!tagGateway) {
      var TagGateway = require("../gateway/tag/httpTagGateway");
      tagGateway = new TagGateway();
    }

    this.tagGateway = tagGateway;
  }

  /**
   * Method gets all projects matching passed query
   * @param  {[type]}   query    [description]
   * @return {[type]}            [description]
   */
  all(query)
  {
    query = query || new Request();
    return this.tagGateway.fetchAll(query)
      .then(function (data) {
        return data || [];
      });
  }

  /**
   * Method gets single project by passed id
   * @param  {[type]}   id       [description]
   * @return {[type]}            [description]
   */
  fetchOne(query)
  {
    return this.all(query)
      .then(function (projects) {
        if (!_.isArray(projects) || _.isEmpty(projects)) {
          return null;
        }
        return projects.shift();
      });
  }

  create(data)
  {
    var project = new TagEntity(data);
    return this.tagGateway.insert(project);
  }

  update(data)
  {
    var project = new TagEntity(data);
    return this.tagGateway.update(project);
  }

  replace(data)
  {
    var project = new TagEntity(data);
    return this.tagGateway.replace(project);
  }

  delete(id)
  {
    return this.tagGateway.delete(id);
  }
}

module.exports = TagsService;
