
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var BaseEntity  = require(appRootPath + "/lib/entity/baseEntity");

class TagEntity extends BaseEntity
{
  constructor(data)
  {
    super();

    this.id = undefined;
    this.name = undefined;
    this.description = undefined;
    this.projectId = undefined;
    this.board = undefined;
    this.type = undefined;
    this.status = undefined;
    this.createdAt = undefined;
    this.updatedAt = undefined;

    if (data) {
      this.inflate(data);
    }
  }

  getId()
  {
    return this.id;
  }

  setId(id)
  {
    this.id = id;
  }
}

module.exports = TagEntity;
