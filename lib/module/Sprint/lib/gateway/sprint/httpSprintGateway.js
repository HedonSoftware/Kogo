
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var HttpGateway  = require(appRootPath + "/lib/gateway/httpGateway");
var SprintEntity = require(appRootPath + "/lib/module/Sprint/lib/entity/sprintEntity");
var _            = require(appRootPath + "/lib/utility/underscore");
var GatewayError = require(appRootPath + "/lib/gateway/gatewayError");
var Request      = require(appRootPath + "/lib/request/request");
var logger       = require(appRootPath + "/lib/logger/logger");

class HttpSprintGateway extends HttpGateway
{
  /**
   * Cutom contractor allows to pass data provider instance
   *
   * @param object httpDataProvider DataProvider's instance(i.e. httpDataProvider)
   */
  constructor(httpDataProvider)
  {
    super(httpDataProvider);
    this.dataProvider.apiUrlParts = [
      "/projects/",
      null,
      "/boards/",
      null,
      "/sprints"
    ];
  }

  /**
   * Method fetches all records matching passed request criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise promise Promise of data from DB
   */
  fetchAll(projectId, boardId, request)
  {
    logger.info("HttpSprintGateway::fetchAll() method called", {
      projectId: projectId,
      boardId: boardId,
      request: request.export()
    });

    this.setupUrl(projectId, boardId);
    return this.dataProvider.sendGetRequest(request);
  }

  save(projectId, boardId, sprintEntity)
  {
    logger.info("HttpSprintGateway::save() method called", {
      projectId: projectId,
      boardId: boardId,
      sprintEntity: sprintEntity
    });

    this.setupUrl(projectId, boardId);
    return super.save(sprintEntity);
  }

  insert(projectId, boardId, sprintEntity)
  {
    logger.info("HttpSprintGateway::insert() method called", {
      projectId: projectId,
      boardId: boardId,
      sprintEntity: sprintEntity
    });

    this.setupUrl(projectId, boardId);

    if (!(sprintEntity instanceof SprintEntity)) {
      throw new GatewayError("Invalid model passed. Instance of SprintEntity expected");
    }

    return super.insert(sprintEntity)
      .then(function(sprintData) {
        return new SprintEntity(sprintData);
      });
  }

  update(projectId, boardId, sprintEntity)
  {
    logger.info("HttpSprintGateway::update() method called", {
      projectId: projectId,
      boardId: boardId,
      sprintEntity: sprintEntity
    });

    this.setupUrl(projectId, boardId);

    if (!(sprintEntity instanceof SprintEntity)) {
      throw new GatewayError("Invalid model passed. Instance of Board expected");
    }

    if (!sprintEntity.getId()) {
      throw new GatewayError("Invalid logic. Insert should be called");
    }

    return super.update(sprintEntity)
      .then(function(boardData) {
        return new SprintEntity(boardData);
      });
  }

  setupUrl(projectId, boardId)
  {
    logger.info("HttpSprintGateway::setupUrl() method called", {
      projectId: projectId,
      boardId: boardId
    });

    this.dataProvider.apiUrlParts[1] = projectId;
    this.dataProvider.apiUrlParts[3] = boardId;
    this.dataProvider.apiUrl = this.dataProvider.apiUrlParts.join("");
  }
}

module.exports = HttpSprintGateway;
