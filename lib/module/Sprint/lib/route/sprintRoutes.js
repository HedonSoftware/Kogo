
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var SprintsService = require("../service/sprintsService");
var logger       = require(appRootPath + "/lib/logger/logger");
var errorHandler = require(appRootPath + "/lib/error/errorHandler");

module.exports.get = function (req, res) {
  logger.info("SprintRoutes::get - request received", {
    httpMethod: req.method,
    url: req.url,
    query: req.query.export()
  });

  var sprintsService = new SprintsService();
  try {
    sprintsService.all(req.params.projectId, req.params.boardId, req.query)
      .then(function (sprints) {
        logger.info("SprintRoutes::get - 200 response sent back", {
          sprints: sprints
        });
        return res.status(200).json(sprints);
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.getById = function (req, res) {
  logger.info("SprintRoutes::getById - request received", {
    httpMethod: req.method,
    url: req.url,
    query: req.query.export()
  });

  var queryConditions = req.query.getConditions();
  queryConditions.id = req.params.sprintId;
  req.query.setConditions(queryConditions);

  var sprintsService = new SprintsService();
  try {
    sprintsService.fetchOne(req.params.projectId, req.params.boardId, req.query)
      .then(function (sprint) {
        if (!sprint) {
          logger.info("SprintRoutes::getId - 404 response sent back");
          return res.status(404).json("Not Found");
        }
        logger.info("SprintRoutes::getId - 200 response sent back", {
          sprint: sprint
        });
        return res.status(200).json(sprint);
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.create = function (req, res) {
  logger.info("SprintRoutes::create - request received", {
    httpMethod: req.method,
    url: req.url,
    body: req.body,
    params: req.params
  });

  var sprintsService = new SprintsService();
  try {
    sprintsService.create(req.params.projectId, req.params.boardId, req.body)
      .then(function (sprint) {
        if (sprint === null) {
          logger.info("SprintRoutes::create - 404 response sent back");
          return res.status(404).json("Not Found");
        }

        var locationHeader = "/projects/" + req.params.projectId + "/boards/" +
                             req.params.boardId + "/sprints/" +  sprint.getId();

        res.setHeader("Location", locationHeader);
        logger.info("SprintRoutes::create - 201 response sent back", {
          locationHeader: locationHeader,
          sprint: sprint
        });
        return res.status(201).json(sprint.export());
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.update = function (req, res) {
  logger.info("SprintRoutes::update - request received", {
    httpMethod: req.method,
    url: req.url,
    body: req.body,
    params: req.params
  });

  req.body.id = req.params.sprintId;

  var sprintsService = new SprintsService();
  try {
    sprintsService.update(req.params.projectId, req.params.boardId, req.body)
      .then(function (sprint) {
        if (sprint === null) {
          logger.info("SprintRoutes::update - 409 response sent back");
          return res.status(409).json("Conflict");
        }
        logger.info("SprintRoutes::update - 200 response sent back", {
          sprint: sprint
        });
        return res.status(200).json(sprint.export());
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.replace = function (req, res) {
  logger.info("SprintRoutes::replace - request received", {
    httpMethod: req.method,
    url: req.url,
    body: req.body,
    params: req.params
  });

  req.body.id = req.params.sprintId;

  var sprintsService = new SprintsService();
  try {
    sprintsService.replace(req.params.projectId, req.params.boardId, req.body)
      .then(function (sprint) {
        if (sprint === null) {
          logger.info("SprintRoutes::replace - 409 response sent back");
          return res.status(409).json("Conflict");
        }
        logger.info("SprintRoutes::replace - 200 response sent back", {
          sprint: sprint
        });
        return res.status(200).json(sprint.export());
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};

module.exports.delete = function (req, res) {
  logger.info("SprintRoutes::delete - request received", {
    httpMethod: req.method,
    url: req.url,
    body: req.body,
    params: req.params
  });

  var sprintsService = new SprintsService();
  try{
    sprintsService.delete(req.params.projectId, req.params.boardId, req.params.sprintId)
      .then(function (numberOfDeleted) {
        if (numberOfDeleted === 0) {
          logger.info("SprintRoutes::delete - 404 response sent back");
          return res.status(404).json("Not Found");
        }
        logger.info("SprintRoutes::delete - 204 response sent back");
        return res.status(204).json("No Content");
      })
      .catch(function (error) {
        return errorHandler.resolve(error, req, res);
      });
  } catch (error) {
    return errorHandler.resolve(error, req, res);
  }
};
