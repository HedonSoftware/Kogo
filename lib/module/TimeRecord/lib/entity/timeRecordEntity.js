
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

var Entity = require(appRootPath + "/lib/entity");
var util  = require("util");

/**
 * TimeLog entity class definition
 */
function TimeLog(data) {
  data = data || false;
  if (data) this.inflate(data);
};

/**
 * TimeLog extends Entity
 */
util.inherits(TimeLog, Entity);

TimeLog.prototype.id = "";
TimeLog.prototype.name = "";
TimeLog.prototype.description = "";
TimeLog.prototype.startDate = "";
TimeLog.prototype.timeLogged = "";
TimeLog.prototype.status = "";
TimeLog.prototype.userId = "";
TimeLog.prototype.ticketId = "";
TimeLog.prototype.createdAt = "";
TimeLog.prototype.updatedAt = "";

TimeLog.prototype.getFields = function () {
  return [
    "id",
    "name",
    "description",
    "startDate",
    "timeLogged",
    "status",
    "userId",
    "ticketId",
    "createdAt",
    "updatedAt"
  ];
};

TimeLog.prototype.getId = function () {
  return this.id;
}

TimeLog.prototype.setId = function (id) {
  this.id = id;
}

module.exports = TimeLog;
