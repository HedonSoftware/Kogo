
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

var TimeLogsService  = require("../service/timeLogs");
var logger              = require(appRootPath + "/lib/logger/logger");
var errorHandler = require(appRootPath + "/lib/error/errorHandler");

exports.get = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var timeLogsService = new TimeLogsService();
  timeLogsService.all(req.query)
    .then(function (timeLogs) {
      return res.status(200).json(timeLogs);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

exports.getById = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var timeLogsService = new TimeLogsService();
  timeLogsService.get(req.params.id)
    .then(function (timeLog) {
      if (!timeLog) return res.status(404).json("Not Found");
      return res.status(200).json(timeLog);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

exports.create = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var timeLogsService = new TimeLogsService();
  timeLogsService.create(req.body)
    .then(function (timeLog) {
      if (timeLog === null) return res.status(404).json("Not Found");
      res.setHeader("Location", "/time-logs/" +  timeLog.getId());
      return res.status(201).json(timeLog.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

exports.update = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var timeLogsService = new TimeLogsService();
  timeLogsService.update(req.body)
    .then(function (timeLog) {
      if (timeLog === null) return res.status(409).json("Conflict");
      return res.status(200).json(timeLog.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

exports.replace = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var timeLogsService = new TimeLogsService();
  timeLogsService.replace(req.body)
    .then(function (timeLog) {
      if (timeLog === null) return res.status(409).json("Conflict");
      return res.status(200).json(timeLog.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

exports.delete = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var timeLogsService = new TimeLogsService();
  timeLogsService.del(req.params.id)
    .then(function (numberOfDeleted) {
      if (numberOfDeleted === 0) return res.status(404).json("Not Found");
      return res.status(204).json("No Content");
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};
