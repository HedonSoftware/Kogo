
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath     = require("app-root-path");
var CommentsService = require("../service/commentsService");
var logger          = require(appRootPath + "/lib/logger/logger");
var errorHandler    = require(appRootPath + "/lib/error/errorHandler");

/**
 * Method returns json response for get multiple comments
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return String response Json response
 */
module.exports.get = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var commentsService = new CommentsService();
  commentsService.fetchAll(req.params.ticketId, req.query)
    .then(function (comments) {
      return res.status(200).json(comments);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.getById = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var commentsService = new CommentsService();
  commentsService.fetchById(req.params.ticketId, req.params.id)
    .then(function (comment) {
      if (!comment) return res.status(404).json("Not Found");
      return res.status(200).json(comment);
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.create = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var commentsService = new CommentsService();
  commentsService.create(req.params.ticketId, req.body)
    .then(function (comment) {
      if (comment === null) return res.status(404).json("Not Found");
      res.setHeader("Location", "/tickets/" + req.params.ticketId + "/comments/" +  comment.getId());
      return res.status(201).json(comment.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

/**
 * Method updates comment record
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
module.exports.update = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var commentsService = new CommentsService();
  commentsService.update(req.params.ticketId, req.body)
    .then(function (comment) {
      if (comment === null) return res.status(409).json("Conflict");
      return res.status(200).json(comment.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

/**
 * Method replaces comment record
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
module.exports.replace = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  req.body.id = req.params.id;

  var commentsService = new CommentsService();
  commentsService.replace(req.params.ticketId, req.body)
    .then(function (comment) {
      if (comment === null) return res.status(409).json("Conflict");
      return res.status(200).json(comment.export());
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};

module.exports.delete = function (req, res) {
  logger.info(req.method + " request: " + req.url);

  var commentsService = new CommentsService();
  commentsService.delete(req.params.ticketId, req.params.id)
    .then(function (numberOfDeleted) {
      if (numberOfDeleted === 0) return res.status(404).json("Not Found");
      return res.status(204).json("No Content");
    })
    .catch(function (error) {
      return errorHandler.resolve(error, req, res);
    });
};
