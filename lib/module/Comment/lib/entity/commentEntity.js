
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
var BaseEntity  = require(appRootPath + "/lib/entity/baseEntity");
var uuid        = require('node-uuid');

class CommentEntity extends BaseEntity
{
  constructor(data)
  {
    super();

    this.id = uuid.v4();
    this.text = undefined;
    this.userId = undefined;
    this.status = undefined;
    this.createdAt = undefined;
    this.updatedAt = undefined;

    if (data) {
      this.inflate(data);
    }
  }

  getId()
  {
    return this.id;
  }

  setId(id)
  {
    this.id = id;
  }
}

module.exports = CommentEntity;
