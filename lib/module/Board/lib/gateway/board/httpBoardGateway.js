
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var HttpGateway  = require(appRootPath + "/lib/gateway/httpGateway");
var BoardEntity  = require(appRootPath + "/lib/module/Board/lib/entity/boardEntity");
var _            = require(appRootPath + "/lib/utility/underscore");
var GatewayError = require(appRootPath + "/lib/gateway/gatewayError");
var Request      = require(appRootPath + "/lib/request/request");
var logger       = require(appRootPath + "/lib/logger/logger");

class HttpBoardGateway extends HttpGateway
{
  /**
   * Cutom contractor allows to pass data provider instance
   *
   * @param object httpDataProvider DataProvider's instance(i.e. httpDataProvider)
   */
  constructor(httpDataProvider)
  {
    super(httpDataProvider);
    this.dataProvider.apiUrlParts = [
      "/projects/",
      null,
      "/boards"
    ];
  }

  /**
   * Method fetches all records matching passed request criteria
   *
   * @param  Request request Used to specify the query
   * @return Promise promise Promise of data from DB
   */
  fetchAll(projectId, request)
  {
    logger.info("HttpBoardGateway::fetchAll() method called", {
      projectId: projectId,
      request: request.export()
    });

    this.setupUrl(projectId);
    return this.dataProvider.sendGetRequest(request);
  }

  save(projectId, board)
  {
    logger.info("HttpBoardGateway::save() method called", {
      projectId: projectId,
      board: board
    });

    this.setupUrl(projectId);
    return super.save(board);
  }

  insert(projectId, boardEntity)
  {
    logger.info("HttpBoardGateway::insert() method called", {
      projectId: projectId,
      boardEntity: boardEntity
    });

    this.setupUrl(projectId);

    if (!(boardEntity instanceof BoardEntity)) {
      throw new GatewayError("Invalid model passed. Instance of Board expected");
    }

    return super.insert(boardEntity)
      .then(function(boardData) {
        return new BoardEntity(boardData);
      });
  }

  update(projectId, boardEntity)
  {
    logger.info("HttpBoardGateway::update() method called", {
      projectId: projectId,
      boardEntity: boardEntity
    });

    this.setupUrl(projectId);

    if (!(boardEntity instanceof BoardEntity)) {
      throw new GatewayError("Invalid model passed. Instance of Board expected");
    }

    if (!boardEntity.getId()) {
      throw new GatewayError("Invalid logic. Insert should be called");
    }

    return super.update(boardEntity)
      .then(function(boardData) {
        return new BoardEntity(boardData);
      });
  }

  setupUrl(projectId)
  {
    logger.info("HttpBoardGateway::setupUrl() method called", {
      projectId: projectId
    });

    this.dataProvider.apiUrlParts[1] = projectId;
    this.dataProvider.apiUrl = this.dataProvider.apiUrlParts.join("");
  }
}

module.exports = HttpBoardGateway;
