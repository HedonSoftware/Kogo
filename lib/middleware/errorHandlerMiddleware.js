
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath  = require("app-root-path");
var logger       = require("../logger/logger");
var _            = require(appRootPath + "/lib/utility/underscore");
var BaseError    = require(appRootPath + "/lib/error/baseError");
var errorHandler = require(appRootPath + "/lib/error/errorHandler");

module.exports = function (error, req, res, next) {

  if (!_.isObject(error)) {
    logger.error("Express error handler caught exception: " + error);
    return res.status(500).json("Internal Server Error");
  }

  logger.error("Express error handler caught exception: " + error.message, error);

  if (error instanceof BaseError) {
    return errorHandler.resolve(error, req, res);
  }

  return res.status(500).json("Internal Server Error");
};
