
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var logger = require('../logger/logger');

module.exports = function (req, res, next) {
  if (!req.session) {
    logger.error('Unable to connect to session storage');
    return res.status(500).send({ code: 'INTERNAL_SERVER_ERROR' });
  }

  // Redis works fine - just continue
  return next();
};
