
# --------------------------------------------------------
# ----------------- Building docker file -----------------
# --------------------------------------------------------
#
# ---
# To build it:
#
# sudo docker build --no-cache -t "hedonsoftware/kogo:2.0.0" .
#
# --
# To spin application:
#
# sudo docker run -itP \
#   --net kogonet \
#   --volume /var/data/kogo/logs:/opt/kogo/logs \
#   --volume /var/data/shared/avatars:/opt/kogo/public/images/avatars \
#   --name hedonsoftware-kogo \
#   --publish 3002:3002 \
#   --detach \
#   --restart=always \
#   hedonsoftware/kogo:2.0.0
#
# ---
# To publish:
#
# sudo docker push hedonsoftware/kogo:2.0.0
#
# ---

FROM    iojs:latest

# Getting newest version of node
RUN     npm install -g n
RUN     n latest

# Install GIT
RUN     apt-get install -y git

# Set GIT to use https
RUN     git config --global url."https://".insteadOf git://

# Bundle app source
COPY    . /opt/kogo

# Install app's npm dependencies
RUN     cd /opt/kogo; npm install --production

# Install bower
RUN     npm install -g bower

# Install app's bower dependencies
RUN     cd /opt/kogo; bower install --allow-root

EXPOSE  3002

CMD ["node", "/opt/kogo/app.js"]
