
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

"use strict";

var appRootPath = require("app-root-path");
// var AccountModule        = require("../lib/module/Account");
// var ApiAccountModule     = require("../lib/module/ApiAccount");
var BoardModule          = require("../lib/module/Board");
// var CommentModule        = require("../lib/module/Comment");
var LaneModule           = require("../lib/module/Lane");
var ProjectModule        = require("../lib/module/Project");
var SprintModule         = require("../lib/module/Sprint");
var TagModule            = require("../lib/module/Tag");
// var TicketActivityModule = require("../lib/module/TicketActivity");
// var TicketLinkModule     = require("../lib/module/TicketLink");
// var TicketStatusModule   = require("../lib/module/TicketStatus");
var TicketModule         = require("../lib/module/Ticket");
// var TimeLogModule        = require("../lib/module/TimeLog");
var UserModule           = require("../lib/module/User");
var logger               = require("../lib/logger/logger");
var sessionCheck         = require("../lib/middleware/sessionMiddleware");

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  res.redirect("/login");
}

function ensureAuthenticatedApi(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  return res.status(403).json("Forbidden");
}

module.exports = function (router, passport) {

  // project resources
  router.get("/api/projects", ensureAuthenticated, ProjectModule.Routes.ProjectRoutes.get);
  router.get("/api/projects/:projectId", ensureAuthenticatedApi, ProjectModule.Routes.ProjectRoutes.getById);

  // board resources
  router.get("/api/projects/:projectId/boards", ensureAuthenticatedApi, BoardModule.Routes.BoardRoutes.get);
  router.get("/api/projects/:projectId/boards/:boardId", ensureAuthenticatedApi, BoardModule.Routes.BoardRoutes.getById);

  // lane resources
  router.get("/api/projects/:projectId/boards/:boardId/lanes", ensureAuthenticatedApi, LaneModule.Routes.LaneRoutes.get);
  router.get("/api/projects/:projectId/boards/:boardId/lanes/:laneId", ensureAuthenticatedApi, LaneModule.Routes.LaneRoutes.getById);

  // sprint resources
  router.get("/api/projects/:projectId/boards/:boardId/sprints", ensureAuthenticatedApi, SprintModule.Routes.SprintRoutes.get);
  router.get("/api/projects/:projectId/boards/:boardId/sprints/:laneId", ensureAuthenticatedApi, SprintModule.Routes.SprintRoutes.getById);
  router.post("/api/projects/:projectId/boards/:boardId/sprints", ensureAuthenticatedApi, SprintModule.Routes.SprintRoutes.create);

  // ticket resources
  router.post("/api/tickets", ensureAuthenticatedApi, TicketModule.Routes.TicketRoutes.create);

  // user resources
  router.get("/api/users", ensureAuthenticated, UserModule.Routes.ApiRoutes.get);
  router.get("/api/users/:id", ensureAuthenticatedApi, UserModule.Routes.ApiRoutes.getById);

  // ------------------------------------------------------------------------------
  // -------------------------------- FRONTEND URLS -------------------------------
  // ------------------------------------------------------------------------------

  // router.get("/ticket-activities", ensureAuthenticated, TicketActivityModule.Routes.Frontend.get);

  router.get("/user", ensureAuthenticated, UserModule.Routes.FrontendRoutes.getDetails);

  router.get("/login", function (req, res) {
    res.status(200).sendFile(appRootPath + "/public/login.html");
  });

  router.post(
    "/login",
    passport.authenticate("local"),
    sessionCheck,
    function (req, res) {
      res.status(200).json("OK");
    }
  );

  router.get("/logout", function (req, res) {
    req.logout();
    res.redirect("/login");
  });

  // load the single view file
  // (angular will handle the page changes on the front-end)
  router.get("*", ensureAuthenticated, function (req, res) {
    res.sendFile(appRootPath + "/public/index.html");
  });
};
