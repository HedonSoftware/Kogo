
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

var lanesService = angular.module('LanesService', []);

lanesService.factory('LanesService', ['$http', function ($http) {

  "use strict";

  var lanesApiUrl = '/api/projects/:projectId/boards/:boardId/lanes';

  return {
    // call to get all lanes
    get : function (projectId, boardId, params) {
      var apiUrl = this.getBaseUrl(projectId, boardId);
      var result = $http.get(
        apiUrl + (params ? '?' + $.param(params) : '')
      );

      // formatting data
      return result.then(function (response) {
        return response.data;
      });
    },

    getById : function (projectId, boardId, id) {
      return this.get(projectId, boardId,
        {
          'conditions': {
            'id': id
          }
        })
        .then(function (lanes) {
          if (!_.isArray(lanes)) {
            return null;
          }

          return lanes.pop();
        });
    },

    save : function (projectId, boardId, lane) {
      if (lane.id) {
        return this.update(projectId, boardId, lane);
      }

      return this.create(projectId, boardId, lane);
    },

    // call to POST and create a new lane
    create : function (projectId, boardId, lane) {
      var apiUrl = this.getBaseUrl(projectId, boardId);
      return $http.post(apiUrl, lane)
        .then(function (result) {
          return result.data;
        });
    },

    update: function (projectId, boardId, lane) {
      var apiUrl = this.getBaseUrl(projectId, boardId);
      return $http.put(apiUrl + '/' + lane.id, lane)
        .then(function (result) {
          return result.data;
        });
    },

    getBaseUrl : function (projectId, boardId) {
      lanesApiUrl = lanesApiUrl.replace(":projectId", projectId);
      return lanesApiUrl.replace(":boardId", boardId);
    }
  };

}]);
