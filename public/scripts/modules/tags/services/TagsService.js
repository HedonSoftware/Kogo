
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

var tagsService = angular.module('TagsService', []);

tagsService.factory('TagsService', ['$http', function ($http) {

  var tagsApiUrl = '/api/tags';

  return {
    // call to get all tags
    get : function (params) {
      var result = $http.get(
        tagsApiUrl + (params ? '?' + $.param(params) : '')
      );

      // formatting data
      return result.then(function (response) {
        return response.data;
      });
    },

    getById : function (id) {
      return this.get(
        {
          'conditions': {
            'id': id
          }
        })
        .then(function (tags) {
          if (!_.isArray(tags)) {
            return null;
          }

          return tags.pop();
        });
    },

    save : function (lane) {
      if (lane.id) {
        return this.update(lane.id, lane);
      }

      return this.create(lane);
    },

    // call to POST and create a new lane
    create : function (lane) {
      return $http.post(tagsApiUrl, lane)
        .then(function (result) {
          return result.data;
        });
    },

    update: function (id, lane) {
      return $http.put(tagsApiUrl + '/' + id, lane)
        .then(function (result) {
          return result.data;
        });
    },

    getByBoardId : function (boardId) {
      return this.get({
        fields: {
          0 : "id",
          1 : "name"
        },
        conditions: {
          boardId : boardId,
          status : 'active'
        }
      });
    }
  };

}]);
