
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

var kogoApp = angular.module(
  'kogoApp',
  [
    // nothing here
  ]
);

kogoApp.controller(
  'LoginController',
  [
    '$scope', 'LoginService', '$window',
    function ($scope, LoginService, $window)
    {
      $scope.login = function (loginForm) {
        LoginService.login(loginForm)
          .then(function () {
            $window.location.href = '/';
          }, function (error) {
            var code = error.data.code;

            if (code === 'INTERNAL_SERVER_ERROR') {
              showAlert(
                'danger',
                '#login-box .alert',
                '<b>Error!</b> Internal server error occurred.'
              );
            } else {
              showAlert(
                'danger',
                '#login-box .alert',
                '<b>Error!</b> Incorrect username or password.'
              );
            }
          });
      }

      // this method is a copy from app.js as we don't want to
      // include the whole app for login only
      $scope.hideAlert = function (alertSelector) {
        var alert = $(alertSelector);
        alert.hide();
      };

      // this method is a copy from app.js as we don't want to
      // include the whole app for login only
      var showAlert = function (alertType, alertSelector, message) {
        var alert = $(alertSelector);
        alert.removeClass().addClass('alert alert-' + alertType + ' alert-dismissable');
        alert.children('.message').html(message);
        alert.show();
      };
    }
  ]
);
