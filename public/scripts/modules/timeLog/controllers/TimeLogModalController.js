
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

var ticketModalController = angular.module(
  'TimeLogModalController',
  [
    'TimeLogsService'
  ]
);

ticketModalController.controller(
  'TimeLogModalController',
  [
    "$scope", "$routeParams", "$q", "$modalInstance",
    "TimeLogsService", "ticket",
    function ($scope, $routeParams, $q, $modalInstance,
      TimeLogsService, ticket
    ) {

      $scope.modalTicket = _.extend({}, ticket);

      // method called to create time log
      $scope.createTimeLog = function (timeLog) {

        if (!_.isObject(timeLog)) {
          throw 'Invalid time log passed';
        }

        return TimeLogsService.save(timeLog);
      }

      // method called to update time log
      $scope.updateTimeLog = function (timeLog) {

        if (!_.isObject(timeLog)) {
          throw 'Invalid time log passed';
        }

        return TimeLogsService.save(timeLog);
      }

      // ---------------------------------------------
      // ---------- MODAL RELEATED FUNCTIONS ---------
      // ---------------------------------------------

      /**
       * Function called when 'create time log' was clicked
       *
       * Role:
       * - validate form input
       *   * if error -> show error
       * - use service to save time log
       *   * if sucess -> close modal
       *   * else -> show error
       */
      $scope.create = function (timeLog) {

        // validation

        // saving time log
        return $scope.createTimeLog(timeLog)
          .then(function (records) {
            $modalInstance.close(records);
          }, function (error) {
            console.log(error);
          });
      };

      /**
       * Function called when 'update time log' was clicked
       *
       * Role:
       * - validate form input
       *   * if error -> show error
       * - use service to save time log
       *   * if sucess -> close modal
       *   * else -> show error
       */
      $scope.update = function (timeLog) {

        // validation

        // saving ticket
        return $scope.updateTimeLog(timeLog)
          .then(function (ticket) {
            $modalInstance.close(timeLog);
          }, function (error) {
            console.log(error);
          });
      };

      /**
       * Cancel method called when 'cancel' button
       * was clicked
       */
      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.activateEditor = function () {
        $('.ta-toolbar').show();
      };

      $scope.deactivateEditor = function () {
        $('.ta-toolbar').hide();
      };
    }
  ]
);
