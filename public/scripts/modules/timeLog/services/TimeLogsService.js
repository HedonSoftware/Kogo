
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

var timeLogsService = angular.module('TimeLogsService', []);

timeLogsService.factory('TimeLogsService', ['$http', function ($http) {

  var timeLogsApiUrl = '/api/time-logs';

  return {
    // call to get all timeLogs
    get : function (params) {
      var result = $http.get(
        timeLogsApiUrl + (params ? '?' + $.param(params) : '')
      );

      // formatting data
      return result.then(function (response) {
        return response.data;
      });
    },

    getById : function (id, params) {
      params = params || {};

      params = _.extend(
        {
          fields : ["name"]
        },
        params
      );

      var query = _.extend(params, {'conditions': {'id': id}});

      return this.get(query)
        .then(function (timeLogs) {
          if (!_.isArray(timeLogs)) {
            return null;
          }

          return timeLogs.pop();
        });
    },

    save : function (timeLog) {
      if (timeLog.id) {
        return this.update(timeLog.id, timeLog);
      }

      return this.create(timeLog);
    },

    // call to POST and create a new timeLog
    create : function (timeLog) {
      return $http.post(timeLogsApiUrl, timeLog)
        .then(function (result) {
          return result.data;
        });
    },

    update: function (id, timeLog) {
      return $http.put(timeLogsApiUrl + '/' + id, timeLog)
        .then(function (result) {
          return result.data;
        });
    },
  }

}]);
