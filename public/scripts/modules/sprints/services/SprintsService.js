
/**
 * Kogo (http://www.kogo.hedonsoftware.com/)
 *
 * @link      https://github.com/HedonSoftware/Kogo for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://github.com/HedonSoftware/Kogo/blob/master/LICENSE.md Proprietary software
 */

var sprintsService = angular.module('SprintsService', []);

sprintsService.factory('SprintsService', ['$http', function ($http) {

  "use strict";

  var sprintsApiUrl = '/api/projects/:projectId/boards/:boardId/sprints';

  return {
    // call to get all sprints
    get : function (projectId, boardId, params) {
      var apiUrl = this.getBaseUrl(projectId, boardId);
      var result = $http.get(
        apiUrl + (params ? '?' + $.param(params) : '')
      );

      // formatting data
      return result.then(function (response) {
        return response.data;
      });
    },

    getById : function (projectId, boardId, id) {
      return this.get(projectId, boardId,
        {
          'conditions': {
            'id': id
          }
        })
        .then(function (sprints) {
          if (!_.isArray(sprints)) {
            return null;
          }

          return sprints.pop();
        });
    },

    save : function (projectId, boardId, sprint) {
      if (sprint.id) {
        return this.update(projectId, boardId, sprint);
      }

      return this.create(projectId, boardId, sprint);
    },

    // call to POST and create a new sprint
    create : function (projectId, boardId, sprint) {
      var apiUrl = this.getBaseUrl(projectId, boardId);
      return $http.post(apiUrl, sprint)
        .then(function (result) {
          return result.data;
        });
    },

    update: function (projectId, boardId, sprint) {
      var apiUrl = this.getBaseUrl(projectId, boardId);
      return $http.put(apiUrl + '/' + sprint.id, sprint)
        .then(function (result) {
          return result.data;
        });
    },

    getBaseUrl : function (projectId, boardId) {
      sprintsApiUrl = sprintsApiUrl.replace(":projectId", projectId);
      return sprintsApiUrl.replace(":boardId", boardId);
    }
  };

}]);
